'use strict';

let nodeGCM = require('node-gcm');
let Token = require('../../token/android');
let async = require('async');
let consts = require('../../consts/consts');
let logUtils = require('../../utils/log.js');
let androidUtils = require('../../utils/android.js');
let config = require('config');
let _ = require('lodash');

class GCM {
  constructor(jobInf, buildInf) {
    this.jobInf = jobInf;
    this.buildInf = buildInf;
    this.maxConcurrent = config.android.gcm.concurrent;
    this.maxTokensPerReq = consts.ANDROID.PER_CONNECT;
    this.queue = null;

    this.result = {
      success: 0,
      fail: 0,
      expires: 0,
      listErrors: [],
      listExpires: [],
      mapCanonical: {}
    };

    this.tokenManager = Token.createTokenManager(buildInf.token.type, jobInf, buildInf);
    this.tokens = [];
    this.isGettingMoreTokens = false;
    this.isKill = false;
  }

  process() {
    return new Promise((resolve, reject) => {
      let apiKey = this.buildInf.apiKey;
      let notification = this.getNotificationPayload();
      let sender = new nodeGCM.Sender(apiKey);

      // create queue for push 
      this.queue = async.queue((job, callback) => {
        logUtils.logInfo(`Push to ${job.tokens.length} tokens`);
        this
          .push(sender, job.tokens, notification)
          .then(r => {
            logUtils.logInfo(`Push result:`, r);

            this.handleResultPush(job.tokens, r);
            callback(null);
          })
          .catch(err => {
            logUtils.logInfo(`Push error:`, err);

            if(!this.isKill) {
              this.isKill = true;
              this.queue.kill();
              reject(err);
            }
            callback(null);
          });
      }, this.maxConcurrent);

      this.queue.drain = () => {
        logUtils.logInfo('Queue drain');

        if(!this.tokenManager.isHasMore() && !this.isGettingMoreTokens) {
          logUtils.logInfo('Push completed: ', this.result);

          if(this.tokenManager.isError()) {
            reject(this.tokenManager.getError());
          } else {
            resolve({
              success: this.result.success,
              fail: this.result.fail,
              expires: this.result.listExpires.length,
              listErrors: this.result.listErrors
            });

            this.clean();
          }
        }
      }

      this.queue.empty = () => {
        logUtils.logInfo(`Queue empty`);

        this.getMoreTokens();
      }

      this.startPush();
    });
  }

  startPush() {
    this.getMoreTokens();
  }

  push(sender, tokens, notification) {
    return new Promise((resolve, reject) => {
      sender.send(notification, { registrationTokens: tokens }, function (err, response) {
        if(err) {
          return reject(err);
        }

        resolve(response);
      });
    });
  }

  handleResultPush(tokens, r) {
    this.result.success += r.success;
    this.result.fail += r.failure;
    r.results.forEach((element, index) => {
      if(element.error) {
        if(element.error === 'NotRegistered'
        || element.error === 'InvalidRegistration'
        || element.error === 'MismatchSenderId') {
          this.result.listExpires.push(tokens[index]);
        } else {
          this.result.listErrors.push(element.error);
        }
      } else {
        if(element.registration_id) {
          this.result.mapCanonical[tokens[index]] = element.registration_id;
        }
      }
    });
  }


  getNotificationPayload() {
    var message = new nodeGCM.Message({
      collapseKey: 'demo',
      priority: 'high',
      contentAvailable: true,
      delayWhileIdle: true,
      dryRun: this.buildInf.runTest ? true : false,
      data: _.clone(this.jobInf.data || {})
    });

    return message;
  }

  getMoreTokens() {
    if(this.tokenManager.isHasMore() && !this.isGettingMoreTokens && !this.isKill) {      
      this.isGettingMoreTokens = true;
      this
        .tokenManager
        .getNext()
        .then(tokens => {
          this.isGettingMoreTokens = false;
          this.addTokens(tokens);
        })
        .catch(err => {
          console.log(err);
          this.isGettingMoreTokens = false;
          this.addTokens([]);
        });
    }
  }

  addTokens(tokens) {
    this.tokens = this.tokens.concat(tokens);
    if(this.tokens.length >= this.maxTokensPerReq) {
      while(this.tokens.length >= this.maxTokensPerReq) {
        let job = this.tokens.splice(0, this.maxTokensPerReq);
        this.queue.push({
          tokens: job
        });
      }

      if((this.queue.length() + this.queue.running()) <= this.maxConcurrent) {
        this.getMoreTokens();
      }
    } else {
      if(this.tokenManager.isHasMore()) {
        this.getMoreTokens();
      } else {
        if(this.tokens.length === 0) {
          this.queue.push([]);
        } else {
          this.queue.push({
            tokens: this.tokens
          });
          this.tokens = [];
        }
      }
    }

    logUtils.logInfo(`Android queue ${this.queue.length()} waiting, ${this.queue.running()} running`);
  }

  clean() {
    if(this.result.listExpires.length !== 0) {
      // remove expire tokens
      // androidUtils.removeTokens(this.jobInf.idApp, this.result.listExpires);
    }
  }
}

module.exports = GCM;