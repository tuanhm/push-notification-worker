'use strict';

let Promise = require('bluebird');
let GCM = require('./gcm');

class Adaptor {
  constructor(jobInf, buildInf) {
    this.jobInf = jobInf;
    this.buildInf = buildInf;
  }

  process() {
    return new Promise((resolve, reject) => {
      let type = this.buildInf.type;
      let instance = null;
      instance = new GCM(this.jobInf, this.buildInf);

      instance
        .process()
        .then(r => {
          resolve(this.handleResult(r));
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  handleResult(r) {
    return {
      success: r.success,
      fail: r.fail,
      expires: r.expires,
      listErrors: this.reduceErrors(r.listErrors),
    };
  }

  reduceErrors(errors) {
    errors = errors || [];
    errors.sort();
    let obj = [];
    if(errors.length !== 0) {
      let currentKey = errors[0];
      let count = 0;
      errors.forEach(error => {
        if(error === currentKey) {
          count++;
        } else {
          obj.push({
            errorMessage: currentKey,
            count: count
          });

          currentKey = error;
          count = 1;
        }
      });

      obj.push({
        errorMessage: currentKey,
        count: count
      });
    }
    return obj;
  }
}

module.exports = Adaptor;