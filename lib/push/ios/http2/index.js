'use strict';

const Promise = require('bluebird');
const config = require('config');
const globalVariables = require('../../../utils/globalVariables.js');
const PushBuild = require('./pushBuild.js');
const async = require('async');
const iosUtils = require('../../../utils/ios.js');
const logUtils = require('../../../utils/log.js');
const consts = require('../../../consts/consts');
const _ = require('lodash');
const util = require('util');

class Http2Push {
  constructor(jobInf) {
    this.jobInf = jobInf;
  }

  process() {
    return new Promise((resolve, reject) => {
      globalVariables
        .get('models')
        .AppleCertKey
        .find({
          idApp: this.jobInf.idApp
        })
        .lean()
        .exec()
        .then(r => {
          logUtils.logInfo(`Number build IOS: ${r.length}`);

          const listHandlers = this.generateHandle(r);
          async.parallel(listHandlers, (err, r) => {
            if(err) {
              return reject(err);
            }
            
            resolve(this.handleResult(r));
          });
        })
        .catch(err => {
          reject(err);
        });
    });
  }


  generateHandle(r) {
    const obj = {};
    r.forEach(build => {
      obj[build.bundleId] = callback => {
        const instance = new PushBuild(this.jobInf, build);
        instance
          .process()
          .then(r => {
            callback(null, {
              err: null,
              r: r
            });
          })
          .catch(err => {
            callback(null, {
              err: err
            });
          });
      }
    });

    return obj;
  }

  handleResult(result) {
    const returnValue = [];
    Object.keys(result).forEach(bundleId => {
      let status = consts.PUSH_RESULT_STATUS.DONE;
      let errorMessage = '';
      let obj = null;

      if(result[bundleId].err) {
        status = consts.PUSH_RESULT_STATUS.ERROR;
        errorMessage = util.inspect(result[bundleId].err);
        obj = {
          bundleId,
          status,
          errorMessage
        };
      } else {
        result[bundleId].r.listErrors = this.reduceErrors(result[bundleId].r.listErrors);

        obj = _.merge({
          bundleId,
          status,
          errorMessage,
        }, result[bundleId].r);
      }
      returnValue.push(obj);
    });

    return returnValue;
  }

  reduceErrors(errors) {
    errors = errors || [];
    errors.sort();
    let obj = [];
    if(errors.length !== 0) {
      let currentKey = errors[0];
      let count = 0;
      errors.forEach(error => {
        if(error === currentKey) {
          count++;
        } else {
          obj.push({
            errorMessage: currentKey,
            count: count
          });

          currentKey = error;
          count = 1;
        }
      });

      obj.push({
        errorMessage: currentKey,
        count: count
      });
    }
    return obj;
  }
}

module.exports = Http2Push;