'use strict';

const Promise = require('bluebird');
const async = require('async');
const apnsManager = require('../../../connections/apns/apns.js');
const iosUtils = require('../../../utils/ios.js');
const logUtils = require('../../../utils/log.js');
const config = require('config');
const apn = require('apn');
let Token = require('../../../token/ios');
let _ = require('lodash');

class PushBuild {
  constructor(jobInf, buildInf) {
    this.jobInf = jobInf;
    this.buildInf = buildInf;
    this.maxConcurrent = config.ios.concurrent;
    this.queue = null;

    this.result = {
      success: 0,
      fail: 0,
      expires: 0,
      listErrors: [],
      listExpires: [],
      shouldDestroy: false
    };

    this.isKill = false;

    this.tokenManager = Token.createTokenManager(buildInf.token.type, jobInf, buildInf);
    this.isGettingMoreTokens = false;
  }

  process() {
    return new Promise((resolve, reject) => {
      this
        .getConnect()
        .then(apnProvider => {
          logUtils.logInfo('Get APNs connection success');
          // get notification payload
          let notification = this.getNotificationPayload();

          // create queue for push 
          this.queue = async.queue((token, callback) => {
            this
              .push(apnProvider, token, notification)
              .then(r => {
                this.handleResultPush(r);

                callback(null);
              })
              .catch(err => {
                if(!this.isKill) {
                  this.isKill = true;
                  this.result.shouldDestroy = true;
                  this.queue.kill();
                  reject(err);

                  this.clean();
                }
                callback(null);
              });
          }, this.maxConcurrent);

          this.queue.drain = () => {
            logUtils.logInfo(`Drain queue`);

            if(!this.tokenManager.isHasMore() && !this.isGettingMoreTokens) {
              logUtils.logInfo(`Push IOS completed: `, this.result);

              if(this.tokenManager.isError()) {
                reject(this.tokenManager.getError());
              } else {
                resolve({
                  success: this.result.success,
                  fail: this.result.fail,
                  expires: this.result.listExpires.length,
                  listErrors: this.result.listErrors
                });

                this.clean();
              }
            }
          }

          this.queue.empty = () => {
            logUtils.logInfo(`Queue empty`);

            this.getMoreTokens();
          }

          this.startPush();
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  startPush() {
    this.getMoreTokens();
  }

  push(apnProvider, token, notification) {
    return new Promise((resolve, reject) => {
      const miliseconds = 30000; // 30 seconds
      let hasCall = false;

      let timeout = setTimeout(() => {
        hasCall = true;
        resolve({
          sent: [],
          failed: [
            {
              device: token,
              status: 408,
              response: {
                reason: 'Timeout'
              }
            }
          ]
        });
      }, miliseconds);

      apnProvider
        .send(notification, token)
        .then(r => {
          if(hasCall) return;
          hasCall = true;
          clearTimeout(timeout);
          resolve(r);
        })
        .catch(err => {
          if(hasCall) return;
          hasCall = true;
          clearTimeout(timeout);
          reject(err);
        });
    });
  }

  getNotificationPayload() {
    var note = new apn.Notification();

    note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
    note.badge = 3;
    note.alert = {
      title: this.jobInf.title,
      body: this.jobInf.content
    };
    note.payload = _.clone(this.jobInf.data || {});
    note.topic = this.buildInf.bundleId;

    return note;
  }

  getConnect() {
    return apnsManager
            .getConnection(this.buildInf);
  }

  handleResultPush(r) {
    this.result.success += r.sent.length;
    this.result.fail += r.failed.length;

    // Handle fails
    r.failed.forEach(r => {
      if(this.isExpire(r)) {
        this.result.listExpires.push(r.device);
      } else {
        if(this.isConnectionExpire(r)) {
          this.result.shouldDestroy = true;
        }

        this.result.listErrors.push(r.response.reason);
      }
    });
  }

  isExpire(r) {
    let expire = false;
    if(r.status === '410' || r.status === '400') {
      expire = true;
    }
    return expire;
  }

  isConnectionExpire(r) {
    let expire = false;

    if(r.status === '403') {
      expire = true;
    }

    return expire;
  }

  getMoreTokens() {
    if(this.tokenManager.isHasMore() && !this.isGettingMoreTokens && !this.isKill) {
      this.isGettingMoreTokens = true;
      this
        .tokenManager
        .getNext()
        .then(tokens => {
          this.isGettingMoreTokens = false;
          this.addTokens(tokens);
        })
        .catch(err => {
          this.isGettingMoreTokens = false;
          this.addTokens([]);
        });
    }
  }

  addTokens(tokens) {
    this.queue.push(tokens);
    logUtils.logInfo(`IOS queue ${this.queue.length()} waiting, ${this.queue.running()} running`);
    if((this.queue.length() + this.queue.running()) <= this.maxConcurrent) {
      this.getMoreTokens();
    }
  }

  clean() {
    logUtils.logInfo(`IOS Clean after push`);
    if(this.result.shouldDestroy) {
      apnsManager.destroyConnection(this.jobInf.idApp, this.buildInf.bundleId);
    }

    if(this.result.listExpires.length !== 0) {
      // iosUtils.removeTokens(this.jobInf.idApp, this.buildInf.bundleId, this.result.listExpires);
    }
  }
}

module.exports = PushBuild;