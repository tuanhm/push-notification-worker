'use strict';

const Promise = require('bluebird');
const config = require('config');
const AdaptorServicePush = require('./android/index.js');
const globalVariables = require('../utils/globalVariables.js');
let logUtils = require('../utils/log.js');

class AndroidPush {
  constructor(jobInf) {
    this.jobInf = jobInf;
  }

  process() {
    return new Promise((resolve, reject) => {
      let start = Date.now();

      globalVariables
        .get('models')
        .AndroidApiKey
        .findOne({
          idApp: this.jobInf.idApp
        })
        .lean()
        .exec()
        .then(r => {
          logUtils.logInfo(r);
          if (!r) {
            return Promise.reject(new Error(`Not found android inf`));
          }

          let instance = new AdaptorServicePush(this.jobInf, r);
          return instance
            .process();
        })
        .then(r => {
          r.start = start;
          r.end = Date.now();
          resolve(r);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}

module.exports = AndroidPush;