'use strict';

const Http2Push = require('./ios/http2');
const Promise = require('bluebird');
const config = require('config');

class IOSPush {
  constructor(jobInf) {
    this.jobInf = jobInf;
  }

  process() {
    return new Promise((resolve, reject) => {
      let instance = null;
      instance = new Http2Push(this.jobInf);

      let start = Date.now();
      instance
        .process()
        .then(r => {
          resolve({
            r,
            start,
            end: Date.now(0)
          });
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}

module.exports = IOSPush;