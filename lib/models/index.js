"use strict";

let fs = require("fs");
let path = require("path");
let models = {};

fs
	.readdirSync(__dirname)
	.filter(function(file) {
		return (file.indexOf(".") !== 0) && (file !== "index.js");
	})
	.forEach(function(file) {
		models[path.basename(file, '.js')] = require('./' + file);
	});

module.exports = models;