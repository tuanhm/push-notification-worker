"use strict";

const mongoConnections = require('../connections/mongodb');
let Schema = require('mongoose').Schema;

let AndroidApiKey = new Schema({
	idApp: { type: Number, unique: true },
	apiKey: { type: String },
	createdAt: { type: Number },
	updatedAt: { type: Number },
  status: { type: Number },
  token: {type: Schema.Types.Mixed },
  runTest: {type: Number}
}, { versionKey: false});

AndroidApiKey.index({idApp: 1});

module.exports = mongoConnections.getConnection('mongodb').model('AndroidApiKey', AndroidApiKey);
