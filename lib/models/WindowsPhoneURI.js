"use strict";

const mongoConnections = require('../connections/mongodb');
const Schema = require('mongoose').Schema;

let WindowsPhoneURI = new Schema({
	idApp: { type: Number },
	URI: { type: String },
  lastActivedAt: { type: Number }
}, { versionKey: false});

WindowsPhoneURI.index({idApp: 1, URI: 1}, {unique: true});

module.exports = mongoConnections.getConnection('mongodb').model('WindowsPhoneURI', WindowsPhoneURI);
