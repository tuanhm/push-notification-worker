"use strict";

const mongoConnections = require('../connections/mongodb');
let Schema = require('mongoose').Schema;

let PushResult = new Schema({
  idPush: { type: Schema.Types.ObjectId },
  status: { type: Number },
  start: {type: Number},
  end: {type: Number},
  ios: {
    status: {type: Number},
    errorMessage: {type: String},
    results: {type: [{
      bundleId: {type: String},
      status: {type: Number},
      errorMessage: {type: String},
      total: { type: Number },
      success: { type: Number },
      fail: { type: Number },
      expires: {type: Number},
      listErrors: {type: [{
        errorMessage: {type: String},
        count: {type: Number}
      }]}
    }]},
    start: { type: Number },
    end: { type: Number },
  },
  android: {
    status: {type: Number},
    errorMessage: {type: String},
    total: { type: Number },
    success: { type: Number },
    fail: { type: Number },
    expires: {type: Number},
    listErrors: {type: [{
      errorMessage: {type: String},
      count: {type: Number}
    }]},
    start: {type: Number },
    end: {type: Number}
  }
}, { versionKey: false});

PushResult.index({idPush: 1}, {unique: true});

module.exports = mongoConnections.getConnection('mongodb').model('PushResult', PushResult);
