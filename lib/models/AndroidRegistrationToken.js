"use strict";

const mongoConnections = require('../connections/mongodb');
const Schema = require('mongoose').Schema;

let AndroidRegistrationToken = new Schema({
  idApp: { type: Number },
  registrationToken: { type: String },
  lastActivedAt: { type: Number }
}, { versionKey: false});

AndroidRegistrationToken.index({idApp: 1, registrationToken: 1}, {unique: true});
AndroidRegistrationToken.index({createdAt: 1});

module.exports = mongoConnections.getConnection('mongodb').model('AndroidRegistrationToken', AndroidRegistrationToken);
