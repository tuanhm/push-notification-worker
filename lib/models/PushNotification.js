"use strict";

const mongoConnections = require('../connections/mongodb');
let Schema = require('mongoose').Schema;

let PushNotification = new Schema({
	idApp: { type: Number },
	oss: { type: Number },
	content: { type: String },
  title: { type: String },
	timeStart: { type: Number },
	status: { type: Number },
	data: {type: Schema.Types.Mixed }
}, { versionKey: false});

PushNotification.index({timeStart: 1});

module.exports = mongoConnections.getConnection('mongodb').model('PushNotification', PushNotification);
