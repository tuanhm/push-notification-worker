"use strict";

const mongoConnections = require('../connections/mongodb');
const Schema = require('mongoose').Schema;

let AppleCertKey = new Schema({
	idApp: { type: Number },
	bundleId: { type: String },
	passphrase: {type: String},
	createdAt: { type: Number },
	updatedAt: { type: Number },
  token: {type: Schema.Types.Mixed },
  runTest: {type: Number}
}, { versionKey: false});

AppleCertKey.index({idApp: 1, bundleId: 1}, {unique: true});

module.exports = mongoConnections.getConnection('mongodb').model('AppleCertKey', AppleCertKey);
