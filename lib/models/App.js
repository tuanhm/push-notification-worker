"use strict";

const mongoConnections = require('../connections/mongodb');
let Schema = require('mongoose').Schema;

let App = new Schema({
	idApp: { type: Number, unique: true },
	name: { type: String },
	createdAt: { type: Number },
	updatedAt: { type: Number }
}, { versionKey: false});

App.index({createdAt: 1});

module.exports = mongoConnections.getConnection('mongodb').model('App', App);
