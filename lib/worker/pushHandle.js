'use strict';

const Promise = require('bluebird');
const IOSPush = require('../push/ios.js');
const AndroidPush = require('../push/android.js');
const consts = require('../consts/consts');
const async = require('async');
const config = require('config');
const util = require('util');
const _ = require('lodash');

class PushHandle {
  constructor(jobInf) {
    this.jobInf = jobInf;
  }

  process() {
    const self = this;
    return new Promise((resolve, reject) => {
      const oss = this.jobInf.oss;
      async.parallel({
        ios: callback => {
          if( (oss&consts.IOS.CODE )=== consts.IOS.CODE ) {
            const instance = new IOSPush(this.jobInf);
            return instance
              .process()
              .then(r => {
                callback(null, {
                  err: null,
                  r: r
                });
              })
              .catch(err => {
                callback(null, {
                  err: err
                });
              });
          }

          let currentTime = Date.now();
          callback(null, {
            err: null,
            r: {
              start: currentTime,
              end: currentTime,
              r: []
            }
          });
        },
        android: callback => {
          if( (oss&consts.ANDROID.CODE) === consts.ANDROID.CODE ) {
            const instance = new AndroidPush(this.jobInf);
            return instance
              .process()
              .then(r => {
                callback(null, {
                  err: null,
                  r: r
                });
              })
              .catch(err => {
                callback(null, {
                  err: err,
                });
              });
          }
          let currentTime = Date.now();

          callback(null, {
            err: null,
            r: {
              start: currentTime,
              end: currentTime,
              success: 0,
              fail: 0,
              expires: 0,
              listErrors: []
            }
          });
        }
      }, (err, r) => {
        if(err) {
          return reject(err);
        }
        console.log(r);
        resolve(self.handleValue(r));
      });
    });
  }

  handleValue(r) {
    r.ios = r.ios || {};
    r.android = r.android || {};

    return {
      ios: this.handleResultIOS(r.ios),
      android: this.handleResultAndroid(r.android)
    }
  }

  handleResultIOS(r) {
    let status = consts.PUSH_RESULT_STATUS.DONE;
    let errorMessage = '';
    let objR = {};
    if(r.err) {
      status = consts.PUSH_RESULT_STATUS.ERROR;
      errorMessage = util.inspect(r.err);
      return {
        status,
        errorMessage
      }
    }

    return {
      status,
      errorMessage,
      start: r.r.start,
      end: r.r.end,
      results: r.r.r
    }
  }

  handleResultAndroid(r) {
    let status = consts.PUSH_RESULT_STATUS.DONE;
    let errorMessage = '';
    if(r.err) {
      status = consts.PUSH_RESULT_STATUS.ERROR;
      errorMessage = util.inspect(r.err);
    }


    let obj = _.merge({
      status,
      errorMessage
    }, r.r);

    return obj;
  }
}

module.exports = PushHandle;