'use strict';

const globalVariables = require('../utils/globalVariables.js');
const consts = require('../consts/consts');
const config = require('config');
const PushHandle = require('./pushHandle');
const Promise = require('bluebird');
const pushUtils = require('../utils/push.js');
const logUtils = require('../utils/log.js');
const fs = require('fs');
const util = require('util');
const _ = require('lodash');

class Worker {
  constructor() {
    this.jobQueue = config.rabbitmq.queueName || 'pntd@123';
    this.channel = null;
    this.init();
  }

  init() {
    const self = this;
    const rabbitMQ = globalVariables.get('rabbitmq').getConnection('rabbitmq');

    rabbitMQ.on('connected', () => {
      this.createChannel();
    });
  }

  createChannel() {
    const rabbitMQ = globalVariables.get('rabbitmq').getConnection('rabbitmq');
    rabbitMQ
      .createChannel()
      .then(channel => {
        channel.prefetch(config.rabbitmq.prefetch);

        console.log('Channel created');

        channel.assertQueue(this.jobQueue, {durable: true});
        this.channel = channel;
        this.registerForMessage();
      })
      .catch(err => {
      });
  }

  registerForMessage() {
    console.log(`Subsribe message at queue: ${this.jobQueue}`);
    this
      .channel
      .consume(this.jobQueue, message => {
        const idJob = message.content.toString();
        logUtils.logInfo('Get job: ', idJob);

        let start = Date.now();
        globalVariables
          .get('models')
          .PushNotification
          .findOne({
            _id: idJob
          })
          .lean()
          .exec()
          .then(r => {
            logUtils.logInfo('Job inf: ', r);

            if(!r) {
              return Promise.reject(new Error('Not found push information'));
            }

            const instance = new PushHandle(r);
            instance
              .process()
              .then(r => {
                logUtils.logInfo('Final push result: ', JSON.stringify(r));
                this.done(null, idJob, message, start, r);
              })
              .catch(err => {
                logUtils.logInfo('Push error: ', err);
                this.done(err, idJob, message, start);
              });
          })
          .catch(err => {
            this.done(err, idJob, message, start);
          });
      });
  }

  done(err, idPush, message, start, r = {}) {
    let status = consts.PUSH_RESULT_STATUS.DONE;
    let errorMessage = '';
    if(err) {
      status = consts.PUSH_RESULT_STATUS.ERROR;
      errorMessage = util.inspect(err);
    }

    pushUtils.updatePushResult(_.merge({
      idPush,
      status,
      errorMessage,
      start,
      end: Date.now()
    }, r));

    this.channel.ack(message);
  }
}

const singletonInstance = new Worker();

module.exports = singletonInstance;