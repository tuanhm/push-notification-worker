'use strict';

let androidUtils = require('../../utils/android.js');
let Promise = require('bluebird');

class Token {
  constructor(jobInf, buildInf) {
    this.jobInf = jobInf;
    this.buildInf = buildInf;

    this.hasMore = true;
    this.errorObject = null;

    this.current = 0;
    this.perpage = 1000;
  }

  getNext() {
    return new Promise((resolve, reject) => {
      androidUtils
        .getTokens(this.jobInf.idApp, this.current, this.perpage)
        .then(tokens => {
          if(tokens.length === 0) {
            this.hasMore = false;
          }

          this.current += this.perpage;
          let temp = [];
          tokens.forEach(e => {
            temp.push(e.registrationToken);
          });
          resolve(temp);
        })
        .catch(err => {
          this.hasMore = false;
          this.errorObject = err;
          reject(err);
        });
    });
  }

  isError() {
    return this.errorObject === null ? false : true;
  }

  getError() {
    return this.errorObject;
  }

  isHasMore() {
    return this.hasMore;
  }
}

module.exports = Token;