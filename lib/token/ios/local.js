'use strict';

let iosUtils = require('../../utils/ios.js');
let Promise = require('bluebird');

class Token {
  constructor(jobInf, buildInf) {
    this.jobInf = jobInf;
    this.buildInf = buildInf;

    this.hasMore = true;
    this.errorObject = null;

    this.current = 0;
    this.perpage = 500;
  }

  getNext() {
    return new Promise((resolve, reject) => {
      iosUtils
        .getTokens(this.jobInf.idApp, this.buildInf.bundleId, this.current, this.perpage)
        .then(tokens => {
          if(tokens.length === 0) {
            this.hasMore = false;
          }

          this.current += this.perpage;
          let temp = [];
          if(this.buildInf.runTest) {
            tokens.forEach(e => {
              temp.push(e.deviceToken + "tuanhm");
            }); 
          } else {
            tokens.forEach(e => {
              temp.push(e.deviceToken);
            }); 
          }
          
          resolve(temp);
        })
        .catch(err => {
          this.hasMore = false;
          this.errorObject = err;
          reject(err);
        });
    });
  }

  isError() {
    return this.errorObject === null ? false : true;
  }

  getError() {
    return this.errorObject;
  }

  isHasMore() {
    return this.hasMore;
  }
}

module.exports = Token;