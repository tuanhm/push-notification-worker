'use strict';

let Local = require('./local');
let Remote = require('./remote');

class Factory {
  constructor() {
  }

  createTokenManager(type, jobInf, buildInf) {
    let instance = null;

    if(type === 'local') {
      instance = new Local(jobInf, buildInf);
    } else if(type === 'remote') {  
      instance = new Remote(jobInf, buildInf);
    }

    return instance;
  }
}

module.exports = new Factory;