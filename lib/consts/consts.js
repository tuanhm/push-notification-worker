module.exports = {
  IOS: {
    CODE: 4,
  },
  WINDOWS_PHONE: {
    PER_CONNECT: 500,
  },
  ANDROID: {
    CODE: 2,
    PER_CONNECT: 1000,
  },
  CONNECTION_STATE: {
    FREE: 0,
    IN_USE: 1
  },
  ID_APP: {
    IONLINE: 8,
    SCTV_SPORT: 9
  },
  PUSH_RESULT_STATUS: {
    PUSHING: 0,
    ERROR: 1,
    DONE: 2
  }
}