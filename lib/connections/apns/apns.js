'use strict';

const Promise = require('bluebird');
const iosUtils = require('../../utils/ios.js');
const apn = require('apn');
const config = require('config');
const logUtils = require('../../utils/log.js');

class APNs {
  constructor() {
    this.list = {}; // {env, connection, lastUsed}
    this.watchConnection();
  }

  getConnection(buildInfo) {
    return new Promise((resolve, reject) => {
      let connection = this.getConnectionFromPool(buildInfo.idApp, buildInfo.bundleId, buildInfo.env);
      if(connection !== null) {
        logUtils.logInfo('Connection from pool');
        resolve(connection);
      } else {
        this
          .createNewConnection(buildInfo)
          .then(apnProvider => {
            resolve(apnProvider);
          })
          .catch(err => {
            reject(err);
          });
      }
    });  
  }

  createNewConnection(buildInfo) {
    logUtils.logInfo(`createNewConnection`, buildInfo);
    return new Promise((resolve, reject) => {
      const idApp = buildInfo.idApp;
      const bundleId = buildInfo.bundleId;

      if(buildInfo.authType === 'certificate') {
        iosUtils
          .getCertAndKey(buildInfo.idApp, buildInfo.bundleId)
          .then(r => {
            const options = {
              cert: r.cert,
              key: r.key,
              passphrase: buildInfo.passphrase,
              production: (buildInfo.env === 'production') ? true : false,
              connectionRetryLimit: 10
            };

            const apnProvider = new apn.Provider(options);

            this.list[idApp] = this.list[idApp] || {};
            this.list[idApp][bundleId] = {
              env: buildInfo.env,
              connection: apnProvider,
              lastUsed: Date.now()
            };

            resolve(apnProvider);
          })
          .catch(err => {
            reject(err);
          });
      } else {
        reject(new Error(`Not supported yet for auth type: ${buildInfo.authType}`));
      }
    });
  }

  destroyConnection(idApp, bundleId) {
    logInfo(`destroyConnection: ${idApp} ${bundleId}`);
    if(this.list[idApp][bundleId]) {
      this.list[idApp][bundleId].shutdown();
      delete this.list[idApp];
    }
  }

  getConnectionFromPool(idApp, bundleId, env) {
    logUtils.logInfo(`getConnectionFromPool: ${idApp} ${bundleId} ${env}`);

    let connection = null;
    if(this.list[idApp] && this.list[idApp][bundleId]) {
      if(this.list[idApp][bundleId].env === env) {
        this.list[idApp][bundleId].lastUsed = Date.now();
        connection = this.list[idApp][bundleId].connection;
      } else {
        // Destroy connection
        this.destroyConnection(idApp, bundleId);
      }
    }

    return connection;
  }

  watchConnection() {
    logUtils.logInfo('Start watchConnection');
    const time = config.ios.expireTime;
    setInterval(() => { 
      // Do st smart here
    }, time);
  }
}

module.exports = new APNs;