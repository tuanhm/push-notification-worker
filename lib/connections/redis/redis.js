"use strict";

const config = require('config');
const redis = require('redis');
const EventEmitter = require('events').EventEmitter;
let logUtils = require('../../utils/logUtils.js');

class Redis extends EventEmitter {
  constructor () {
    super();

    this.conn = null;
  }

  connect() {
    let port = config.redis.port;
    let host = config.redis.host;
    let password = config.redis.password;
    let database = config.redis.database;

    this.conn = redis.createClient({
      host: host,
      port: port,
      db: database,
      password: password
    });

    this.conn.on('connect', () => {
      this.emit('connect');
    });

    this.conn.on('error', err => {
      this.emit('error', err);
    });

    this.conn.on('end', () => {
      this.emit('end');
    });
  }

  getConnection() {
    return this.conn;
  }
}

const singletonInstance = new Redis();
singletonInstance.connect();

module.exports = singletonInstance;
