"use strict";

const config = require('config');
const amqp = require('amqplib');
const EventEmitter = require('events').EventEmitter;
const rabbitmqUtils = require('../../utils/rabbitmq.js');

class RabbitMQ extends EventEmitter {
  constructor () {
    super();

    this.url = rabbitmqUtils.getConnectString(config.rabbitmq);

  }

  connect() {
    amqp
      .connect(this.url)
      .then(conn => {
        this.conn = conn;
        this.createChannel();

        this.emit('connected');
        // Listen for some events
        conn.on("close", () => {
          this.emit('close');
        });

        conn.on('error', err => {
          this.emit('error', err);
        });
      })
      .catch(err => {
        // Do st smart here
        console.log(err);
      });
  }

  createChannel() {
    return this
      .conn
      .createConfirmChannel();
  }

  getConnection() {
    return this.conn;
  }
}

const rabbitMQ = new RabbitMQ;
rabbitMQ.connect();

module.exports = rabbitMQ;
