'use strict';

'use strict';

const fs = require('fs');
const util =require('util');

const connections = {};

fs
  .readdirSync(__dirname)
  .filter(fileName => {
    return fileName !== 'index.js';
  })
  .forEach(fileName => {
    const connectionName = fileName.slice(0, -3);
    connections[connectionName] = require(`./${fileName}`);

    connections[connectionName]
      .on('connected', () => {
        console.log(`Mongodb Connected: ${fileName}`);
      })
      .on('disconnected', () => {
        console.log(`Mongodb Disconnected: ${fileName}`);
      })
      .on('error', err => {
        console.log(`Mongodb Error: ${fileName} ${util.inspect(err)}`);
      });
  });

module.exports = {
  getConnection: name => {
    return connections[name];
  }
}


