'use strict';

const config = require('config');
const mongoose = require('mongoose');
const bluebird = require('bluebird');
const mongodbUtils = require('../../utils/mongodb.js');

mongoose.Promise = require('bluebird');

let connectString = mongodbUtils.getConnectString(config.mongodb);

let model = mongoose.createConnection(connectString, {
  server: {
    reconnectTries: 999999,
    auto_reconnect: true,
  },
});

module.exports = model;
