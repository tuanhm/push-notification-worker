'use strict';
const _ = require('lodash');
const validator = require('validator');

module.exports = {
  getConnectString: obj => {
    const connectStr = `amqp://${obj.username}:${obj.password}@${obj.host}:${obj.port}/${obj.vitualHost}`;
    console.log(`ConnectStr: ${connectStr}`);
    return connectStr;
  }
}