'use strict';

let globalVariables = require('./globalVariables.js');
let async = require('async');
let Promise = require('bluebird');
let logUtils = require('./log');

module.exports = {
  removeTokens: (idApp, tokens) => {
     globalVariables
      .get('models')
      .AndroidRegistrationToken
      .remove({
        idApp,
        deviceToken: {
          '$in': tokens
        }
      })
      .then(r => {
        logUtils.logInfo(`Android remove ${tokens.length} expire tokens`);
      })
      .catch(err => {
        logInfo.logInfo(`Android remove expire tokens fail`);
      });
  },
  getTokens: (idApp, start, perpage) => {
    logUtils.logInfo(`Android getTokens ${idApp} ${start} ${perpage}`);

    return globalVariables
      .get('models')
      .AndroidRegistrationToken
      .find({
        idApp
      }, {
        _id: 0,
        registrationToken: 1
      })
      .skip(start)
      .limit(perpage)
      .lean()
      .exec();
  }
}