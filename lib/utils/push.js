'use strict';

const globalVariables = require('./globalVariables');
const util = require('util');
const _ = require('lodash');
const logUtils = require('./log');

module.exports = {
  updatePushResult: result => {
    globalVariables
      .get('models')
      .PushResult
      .create(result)
      .then(r => {
        logUtils.logInfo('Insert to mongodb success');
      })
      .catch(err => {
        logUtils.logInfo('Insert to mongodb fail: ', err);
      });
  },
  
}