'use strict';

const globalVariables = require('./globalVariables.js');
const async = require('async');
const Promise = require('bluebird');
const config = require('config');
const rp = require('request-promise');
let logUtils = require('./log');

module.exports = {
  getCertAndKey: (idApp, bundleId) => {
    return new Promise((resolve, reject) => {
      async.parallel({
        cert: callback => {
          const url = `${config.cmsUrl}/static/apns/${idApp}/${bundleId}.cert`;
          rp(url)
            .then(r => {
              callback(null, r);
            })
            .catch(err => {
              callback(err);
            });
        },
        key: callback => {
          const url = `${config.cmsUrl}/static/apns/${idApp}/${bundleId}.key`;
          rp(url)
            .then(r => {
              callback(null, r);
            })
            .catch(err => {
              callback(err);
            });
        }
      }, (err, r) => {
        if(err) {
          return reject(err);
        }

        resolve(r);
      });
    });
  },
  getTokens: (idApp, bundleId, start, perpage) => {
    logUtils.logInfo(`IOS getTokens ${idApp} ${bundleId} ${start} ${perpage}`);
    
    return globalVariables
      .get('models')
      .AppleDeviceToken
      .find({
        idApp,
        bundleId
      }, {
        _id: 0,
        deviceToken: 1
      })
      .skip(start)
      .limit(perpage)
      .lean()
      .exec();
  },
  removeTokens: (idApp, bundleId, tokens) => {
    globalVariables
      .get('models')
      .AppleDeviceToken
      .remove({
        idApp,
        bundleId,
        deviceToken: {
          '$in': tokens
        }
      })
      .then(r => {
        logUtils.logInfo(`IOS remove ${tokens.length} expire tokens`);
      })
      .catch(err => {
        logInfo.logInfo(`IOS remove expire tokens fail`);
      });
  }
}