'use strict';
const _ = require('lodash');
const validator = require('validator');

module.exports = {
  getConnectString: obj => {
    const host = obj.host;
    const port = obj.port;
    const database = obj.database;
    const username = obj.username;
    const password = obj.password;

    let userPath = '';
    if(username && password) {
      userPath = `${username}:${password}@`;
    }

    const connectStr = `mongodb://${userPath}${host}:${port}/${database}`;
    console.log(`ConnectStr: ${connectStr}`);
    return connectStr;
  }
}