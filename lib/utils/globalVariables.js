'use strict';

const data = {};

module.exports = {
  get: key => {
    return data[key];
  },
  set: (key, value) => {
    data[key] = value;
  }
}