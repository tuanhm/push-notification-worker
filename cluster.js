'use strict';

let cluster = require('cluster');
let numCPUs = require('os').cpus().length;
let config = require('config');

if (cluster.isMaster) {
  let numWorkers = config.numWorkers || numCPUs;
  // Fork workers.
  for (var i = 0; i < numWorkers; i++) {
    cluster.fork();
  }
} else {
  require('./index');
}

process.on('uncaughtException', err => {
  console.log('uncaughtException', err);
});