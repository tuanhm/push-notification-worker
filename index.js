'use strict';

const globalVariables = require('./lib/utils/globalVariables.js');
const fs = require('fs');
const winston = require('winston');

// Logger
let logSystemDir = __dirname + '/system-logs';
fs.existsSync(logSystemDir) || fs.mkdirSync(logSystemDir);
const logger = new winston.Logger({
  levels: {
    info: 1,
    error: 0,
  },
  colors: {
    info: 'green',
    error: 'red'
  },
  transports: [
    new (winston.transports.Console)({
      level: 'info',
      colorize: true
    }),
    new (require('winston-daily-rotate-file'))({
      level: 'error',
      datePattern: 'dd-MM-yyyy',
      filename: logSystemDir + '/system-',
      json: false,
      timestamp: function () {
        return (new Date()).toLocaleString();
      },
    })
  ]
});

// Set global variables
globalVariables.set('mongodb', require('./lib/connections/mongodb'));
globalVariables.set('models', require('./lib/models'));
globalVariables.set('rabbitmq', require('./lib/connections/rabbitmq'));
globalVariables.set('logger', logger);

// Start worker
require('./lib/worker');

process.on('uncaughtException', err => {
  console.log('uncaughtException', err);
});
